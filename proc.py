#!/usr/bin/env python
import cv2
import circ
import os
import os.path
import sys

draw = False

def pixels_to_um(pixels): return pixels * 5.0/30
def circle_to_txt(c): return "%.2f" % (pixels_to_um(2*c[2]))

def draw_methods(img):
    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    methods_colors = [
        (circ.circ_blur, (0, 255, 0)),
        (circ.circ_hp, (255, 0, 0)),
        (circ.circ_edge, (0, 0, 255)),
        (circ.circ_hpedge, (255, 0, 255))
    ]
    circlist = [ method(img) for method, _ in methods_colors ]
    simlist, avgs = circ.filter_circles(circlist)
    
    
    cc = [ (circles, mc[1]) for circles, mc in zip(simlist, methods_colors) ]
    for circles, color in cc:
        #circ.draw_circles(cimg, circles, color)
        pass


    circ.draw_circles(cimg, avgs, (0, 255, 255), circle_to_txt)
    
    
    return cimg, avgs, circlist


def getcircles(filename):
    print filename.replace(' ', '_'), 
    img = cv2.imread(filename, 0)
    img = cv2.resize(img, (0, 0), fx=0.25, fy=0.25)

    #cimg, avgs, _ = draw_methods(img)
    avgs,_ = circ.circ_combo(img)

    cimg = cv2.cvtColor(img, cv2.COLOR_GRAY2BGR)
    circ.draw_circles(cimg, avgs, (0, 255, 255), circle_to_txt)

    for c in avgs:
        print circle_to_txt(c),
    print
    wfile = 'detected/' + filename
    wdir = os.path.dirname(wfile)
    if not os.path.exists(wdir): os.makedirs(wdir)
    cv2.imwrite(wfile, cimg)


files_start = 1
filenames = sys.argv[files_start:]
for fn in filenames:
    getcircles(fn)

