import numpy as np
import cv2
import cv2.cv as cv
import sys
import matplotlib.pyplot as plt
import scipy.spatial

MINRAD=8
MAXRAD=140
MINDIST=50

SIMILAR_MAXDIST=20

LARGE = []
    
def meanf(num, mean):
    global LARGE
    if num < mean:
        LARGE.append(num)
        return num
    else:
        #return np.uint8(128)
        return 128
    
vectf = np.vectorize(meanf)
    
def high_pass(img, count=3):
    global LARGE
    im = vectf(img, img.mean())
    
    for i in range(count):
        mn = np.average(LARGE)
        LARGE = []
        im = vectf(im, mn)
    return im
    
    
def get_circles(img):
    circles = cv2.HoughCircles(img, cv.CV_HOUGH_GRADIENT, 1, MINDIST, param1=50, param2=30, minRadius=MINRAD, maxRadius=MAXRAD)
    if circles is None: return []
    return [tuple(c) for c in circles[0]]

def get_edge(img):
    kernel = np.array([ 
        [0.0, 1.0, 0.0], 
        [1.0, 0.0, -1.0], 
        [0.0, -1.0, -0.0]
    ],np.float32)
    
    new_img = cv2.filter2D(img,-1,kernel)
    
    for i in [1,2,3]:
        kernel = np.array(np.rot90(kernel).tolist())
        new_img = new_img + cv2.filter2D(img, -1, kernel)
        
     
    return new_img

def draw_circles(img, circles, color=(0,255,0), textfunc=None):
    for c in circles:
        # draw the outer circle
        cv2.circle(img,(c[0],c[1]),c[2],color,1)
        # draw the center of the circle
        cv2.circle(img,(c[0],c[1]),2,color,3)
        if textfunc:
            tx = int(c[0] + c[2] + 5)
            ty = int(c[1])
            text = textfunc(c)
            cv2.putText(img, text, (tx, ty), 0, 0.5, color)

def circles_are_similar(c1, c2, maxdist=15):
    cdist = scipy.spatial.distance.euclidean(c1, c2)
    #mdnorm = maxdist * c1[2]/30
    mdnorm = maxdist + c1[2] * 0.30
    return cdist < mdnorm
    #return abs(c1[0]-c2[0]) < maxdist and abs(c1[1]-c2[1]) < maxdist and abs(c1[2]-c2[2]) < maxdist*2


def find_similar_circle(circle, clist, maxdist=SIMILAR_MAXDIST):
    return [candidate for candidate in clist if circles_are_similar(candidate, circle, maxdist)]
        
def filter_circles(clist, minimum=None, maxdist=SIMILAR_MAXDIST):
    if not minimum:
        minimum = int( len(clist)*0.75 )

    total_list = [ circle for lst in clist for circle in lst ]
    similars = {}
    for c in total_list:
        similars[c] = find_similar_circle(c, total_list, maxdist)
    
    simset = set(circle for circle, sims in similars.iteritems() if len(sims) >= minimum)
    filt_list = [ set(lst) & simset for lst in clist]
    filt_set = set( frozenset(sims) for _,sims in similars.iteritems() if len(sims) >= minimum )
    avgs = [ avg_circle(circles) for circles in filt_set ]
    return filt_list, avgs
    
def avg_circle(circles):
    return tuple( np.mean(list(circles), axis=0) )
        



def circ_hp(img, count=3):
    img = high_pass(img, count)
    return get_circles(img)

def circ_edge(img):
    img = get_edge(img)
    return get_circles(img)

def circ_hpedge(img):
    img = high_pass(img, 0)
    img = get_edge(img)
    return get_circles(img)
    
def circ_blur(img):
    circles = []
    tries = 10
    for i in range(tries):
    
        circles = get_circles(img)
        
        if len(circles) <= 0:
            blr = cv2.GaussianBlur(img, (5,5), 5)
            img = cv2.addWeighted(img, 1.5, blr, -0.5, 0)
            continue
            
        #print len(circles[0]) 
        if len(circles) > 2:
            img = cv2.GaussianBlur(img, (5,5), 5)
            continue
            
        else: 
            break
            
    return circles

def circ_combo(img):
    methods = [
        circ_blur,
        circ_hp,
        circ_edge,
        circ_hpedge
    ]
    circlist = [ method(img) for method in methods ]
    simlist, avgs = filter_circles(circlist)
    
    return avgs, circlist
