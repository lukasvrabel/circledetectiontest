#!/usr/bin/env python
import sys
import numpy as np
import cv2
import circ
import collections


def pixels_to_um(pixels): return pixels * 5.0/30
def circle_to_txt(c): return "%.2f" % (pixels_to_um(2*c[2]))


def read_frame(cap):
    _, img = cap.read()

    img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    img = cv2.resize(img, (0,0), fx=0.25, fy=0.25)
    return img


filename = sys.argv[1]
cap = cv2.VideoCapture(filename)

# Define the codec and create VideoWriter object
fourcc = cv2.cv.CV_FOURCC(*'PIM1')
out = cv2.VideoWriter('output.avi',fourcc, 20.0, (480, 270))
QUEUE_LEN=10
clist = collections.deque(maxlen=QUEUE_LEN)
framebuff = collections.deque(maxlen=QUEUE_LEN/2)

framerads = []
lastsec = 0
while(cap.isOpened()):
    
    img = read_frame(cap)
    framebuff.append(img)
    

    newcircs = circ.circ_blur(img)
    #newcircs = circ.circ_hp(img)
    #newcircs,_ = circ.circ_combo(img)

    clist.append(newcircs)
    _, avgs = circ.filter_circles(clist, len(clist))
    #if len(avgs) > 0:
    #    clist.append(avgs)


    cimg = cv2.cvtColor(framebuff[0], cv2.COLOR_GRAY2BGR)
    circ.draw_circles(cimg, avgs, textfunc=circle_to_txt)
    out.write(cimg)

    time = int(cap.get(0))
    ratio = cap.get(2)
    print "frame: %.2f %d" % (ratio, time), 
    for c in avgs: print circle_to_txt(c),
    print

    rads = [ pixels_to_um(c[2]) for c in avgs ]
    if len(rads) > 0: framerads.append(np.mean(rads))

    if time / 1000 != lastsec:
        lastsec = time/1000
        print "mean:", lastsec, 2*np.mean(framerads), 2*np.std(framerads)
        framerads = []
        
        #comboavgs, _ = circ.circ_combo(img)
        #print 'combo:', lastsec,
        #for c in comboavgs: print circle_to_txt(c),
        #print

    cv2.imshow('frame',cimg)
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

print 
cap.release()
#out.release()
cv2.destroyAllWindows()

